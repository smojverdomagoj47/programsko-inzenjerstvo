def uppercase_count(string):
    count = 0
    allowed_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    for char in string:
        if char in allowed_chars:
            count += 1

    return count

def lowercase_count(string):
    count = 0
    allowed_chars = "abcdefghijklmnopqrstuvwxyz"

    for char in string:
        if char in allowed_chars:
            count += 1

    return count

def number_count(string):
    count = 0
    allowed_chars = "123456789"

    for char in string:
        if char in allowed_chars:
            count += 1

    return count

str = input("Unesi string: ")
print(uppercase_count(str))
print(lowercase_count(str))
print(number_count(str))




