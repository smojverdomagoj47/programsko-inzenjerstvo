from query_handler_base import QueryHandlerBase
import random
import requests
import json

class DarkJokeHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "dark" in query:
            return True
        return False

    def process(self, query):
        names = query.split()
        term = names

        try:
            result = self.call_api(term)
            self.ui.say("Hope you found what you were looking for")
            self.ui.say(f"Your word was {term}")
        except: 
            self.ui.say("Oh no! There was an error trying to contact urban dictionary api.")
            self.ui.say("Try something else!")



    def call_api(self, term):
        url = 'https://dark-humor-jokes.p.rapidapi.com/joke'

        querystring = {"term":term}

        headers = {
	'X-RapidAPI-Key': '009660058amsh0c4cae078196acbp1fb19bjsnb07a156f84d4',
    'X-RapidAPI-Host': 'dark-humor-jokes.p.rapidapi.com'
}

        response = requests.request("GET", url, headers=headers)

        print(response.text)
