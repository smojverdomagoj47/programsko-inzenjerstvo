from query_handler_base import QueryHandlerBase
import random
import requests
import json

class MovieHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "movie" in query:
            return True
        return False

    def process(self, query):       
        name = query.split()
        movieName = name[1]
        
        try:
            result = self.call_api(movieName)
            title = result("Title")
            year = result("Year")
            self.ui.say(f"Title: {title}\nYear: {year}")
        except: 
            self.ui.say("Oh no! There was an error trying to contact Movie api.")
            self.ui.say("Try something else!")



    def call_api(self, movieName):
        url = "https://movie-database-alternative.p.rapidapi.com/"

        querystring = {"s":movieName}

        headers = {
            "X-RapidAPI-Key": "009660058amsh0c4cae078196acbp1fb19bjsnb07a156f84d4",
            "X-RapidAPI-Host": "movie-database-alternative.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers, params=querystring)


        return json.loads(response.text)

